## Problème
- *Raison de cette PR*
- *Tickets concernés* : #NUMÉRO

## Solution
- *Et comment vous résolvez ce problème*

## État de la PR
- [ ] Code terminé.
- [ ] Correction ou amélioration testées.
- [ ] Mise à jour depuis la dernière version testée.
- [ ] Peut être revue et testée.

## Validation
---
- [ ] **Revue du code** : 
- [ ] **Accord (LGTM)** :  