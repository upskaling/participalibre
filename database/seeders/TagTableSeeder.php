<?php

namespace Database\Seeders;

use App\Models\Tag;
use App\Models\Project;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed tags
        Tag::factory()->count(15)->create();
        // Attach tag/projects
        Project::all()->each(function($project) {
            $project->tags()->attach([
                rand(1,15),
                rand(1,15),
            ]);
        });

    }
}
