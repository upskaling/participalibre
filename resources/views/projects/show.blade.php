@extends('layouts.app')

@section('title', $project->name)

@section('content')
<section id="show">
  <header>
    <div class="columns flex-wrap">
      <div class="category text-large">{{ $project->tags()->first()->category->name }}</div>
      @foreach ($project->tags as $tag)
      <div class="tag text-large">{{ $tag->name }}</div>
      @endforeach
    </div>
    <h1>{{ $project->name }}</h1>
    <p>Publiée le {{ $project->created_at->format('d/m/Y à h:i') }}</p>
  </header>
  <section class="child-margin-vertical-medium width-1-2">
    <div>
      <h2 class="text-normal margin-none">Description</h2>
      <p class="margin-none"> {{ $project->description }}</p>
    </div>

    <div>
      <h2 class="text-normal margin-none">Lien</h2>
      <a href="{{ $project->url }}" class="text-normal text-primary">{{ $project->url }}</a>
    </div>

    <div>
      <h2 class="text-normal margin-none">Licence</h2>
      <p class="margin-none text-normal text-secondary">{{ $project->licence }}</p>
    </div>

    <div>
      <h2 class="text-normal margin-none">Détails de la contribution souhaitée</h2>
      <p class="margin-none">{{ $project->details }}</p>
    </div>

    <div>
      <h2 class="text-normal margin-none">Rémunération</h2>
      <p class="margin-none">{{ $project->remuneration }}</p>
    </div>

    <div>
      <h2 class="text-normal margin-none">Contact</h2>
      <p class="margin-none">{{ $project->creator }} : <a class="text-primary"
          href="mailto:{{ $project->email }}">{{ $project->email }}</a></p>
    </div>
  </section>
  <div class="project-actions">
    <div class="width-1-2">
      <button class="button-danger icon-danger">Signaler l’annonce</button>
    </div>
    <div class="width-1-2">
      <a href="/" class="link">Retour à l’accueil</a>
    </div>
  </div>
</section>
@endsection
