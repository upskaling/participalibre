@extends('layouts.app')

@section('title', 'Confirmation')

@section('content')
<section>
  <h1 class="margin-none">Félicitations, votre demande de contribution a été publiée !</h1>
  <h2 class="margin-none">
    Un lien unique pour modifier votre annonce arrivera prochainement. En attendant, pour modifier votre annonce, contactez-nous sur
    bonjour@contribulle.org.
  </h2>
  <div class="margin-medium-top columns flex-column flex-middle width-1-2 margin-auto">
    <div>
      <h2 class="text-normal">Que souhaitez-vous faire maintenant ?</h2>
    </div>
    <div>
      <a href="/projects/{{ $project_id }}" class="tile height-medium margin-small">
        <h2>Voir mon annonce publiée</h2>
      </a>
    </div>
    <div>
      <a href="{{ route('projects.create') }}" class="tile height-medium margin-small">
        <h2>Créer une nouvelle demande de contribution</h2>
      </a>
    </div>
  </div>
  <div class="margin-medium-top text-center">
    <a class="link" href="{{ route('home') }}">Retour à l’accueil</a>
  </div>
</section>
@endsection