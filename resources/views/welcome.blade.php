@extends('layouts.app')

@section('title', 'Bienvenue')

@section('content')

@if (session('delete'))
<div class="inverse">
    <p class="icon-danger text-center margin-none padding-small">
        Votre projet a bien été supprimé
    </p>
</div>
@endif
    <section class="home">
        <div class="columns flex-wrap">
            <div class="column">
                <a href="{{ route('projects.create') }}" class="tile">
                    <h2>J'ai un projet</h2>
                    <span class="desc">
                        Et j'aimerais avoir un coup de main
                    </span>
                </a>
            </div>
            <div class="column">
                <a href="{{ route('projects.index') }}" class="tile">
                    <h2>Je veux contribuer</h2>
                    <span class="desc">
                        J'ai envie de participer à l'évolution d'un outil
                    </span>
                </a>
            </div>
        </div>
    </section>
@endsection
